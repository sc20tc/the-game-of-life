#include <stdio.h>
#include <stdlib.h>
#include "logic.h"
#include "files.h"
#include <SDL2/SDL.h>
#include "unity.h"



void test_newGrid() {
  Grid g1 = newGrid(0,1); TEST_ASSERT_EQUAL(g1.rows, 0); TEST_ASSERT_EQUAL(g1.cols, 0);
  Grid g2 = newGrid(-1,-1); TEST_ASSERT_EQUAL(g2.rows, 0); TEST_ASSERT_EQUAL(g2.cols, 0);
  Grid g3 = newGrid(5,10); TEST_ASSERT_EQUAL(g3.rows, 5); TEST_ASSERT_EQUAL(g3.cols, 10);
  Grid g4 = newGrid(72,11); TEST_ASSERT_EQUAL(g4.rows, 72); TEST_ASSERT_EQUAL(g4.cols, 11);
  Grid g5 = newGrid(100,99); TEST_ASSERT_EQUAL(g5.rows, 100); TEST_ASSERT_EQUAL(g5.cols, 99);
  Grid g6 = newGrid(101,60); TEST_ASSERT_EQUAL(g6.rows, 0); TEST_ASSERT_EQUAL(g6.cols, 0);
}

void test_nextGen() {
  Grid g1 = newGrid(3,3);
  g1.arr[1][1] = 1;
  nextGen(g1);
  TEST_ASSERT_EQUAL(g1.arr[1][1], 0);
  TEST_ASSERT_EQUAL(g1.arr[0][0], 0);
  TEST_ASSERT_EQUAL(g1.arr[0][1], 0);
  TEST_ASSERT_EQUAL(g1.arr[0][2], 0);
  TEST_ASSERT_EQUAL(g1.arr[1][0], 0);
  TEST_ASSERT_EQUAL(g1.arr[1][2], 0);
  TEST_ASSERT_EQUAL(g1.arr[2][0], 0);
  TEST_ASSERT_EQUAL(g1.arr[2][1], 0);
  TEST_ASSERT_EQUAL(g1.arr[2][2], 0);

  // for this test I will be testing the "glider" example that is shown in the coursework assignmenet pdf after 3 generations
  Grid g2 = newGrid(5,5);
  g2.arr[2][0] = 1;
  g2.arr[3][1] = 1;
  g2.arr[3][2] = 1;
  g2.arr[2][2] = 1;
  g2.arr[1][2] = 1;

  nextGen(g2);
  nextGen(g2);
  nextGen(g2);

// for my game a cell is alive if it is greater than or equal to 1. Therefore if it is alive I will set it equal to 1 for testing
  if (g2.arr[2][1] >= 1) g2.arr[2][1] = 1;
  if (g2.arr[3][2] >= 1) g2.arr[3][2] = 1;
  if (g2.arr[4][2] >= 1) g2.arr[4][2] = 1;
  if (g2.arr[3][3] >= 1) g2.arr[3][3] = 1;
  if (g2.arr[2][3] >= 1) g2.arr[2][3] = 1;

  TEST_ASSERT_EQUAL(g2.arr[2][1], 1);
  TEST_ASSERT_EQUAL(g2.arr[3][2], 1);
  TEST_ASSERT_EQUAL(g2.arr[4][2], 1);
  TEST_ASSERT_EQUAL(g2.arr[3][3], 1);
  TEST_ASSERT_EQUAL(g2.arr[2][3], 1);

}



void setUp(){

}

void tearDown(){

}

int main() {
  UNITY_BEGIN();
  RUN_TEST(test_newGrid);
  RUN_TEST(test_nextGen);

  return UNITY_END();
}
