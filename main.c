#include <stdio.h>
#include <stdlib.h>
#include "logic.h"
#include "files.h"
#include <SDL2/SDL.h>

//compile command gcc main.c files.c logic.c -lSDL2 -lSDL2_image -std=c99 -o game

void main()
{
    int input;
    printf("1. Generate random world\n2. Load saved world\n3. Exit: ");
    scanf(" %i", &input);
    int iterations = 0;
    if (input == 1) {
      int rows;
      printf("Enter number of rows and columns for your world: ");
      scanf(" %i", &rows);
      printf("How many iterations of the game would you like to run: ");
      scanf(" %i", &iterations);
      Grid grid = randomWorld(rows, rows);
      if (iterations <= 0) {
        printf("You must have a positive number of iterations. ");
        exit(1);
      }
      runGame(grid, iterations);
      saveWorld(grid);
    }

    else if (input == 2) {
      Grid grid = readState("saved.txt");
      printf("How many iterations would you like to perform? ");
      scanf(" %i", &iterations);
      if (iterations <= 0) {
        printf("You must have a positive number of iterations. ");
        exit(1);
      }
      runGame(grid, iterations);
      saveWorld(grid);
    }
    else if (input == 3) {
      exit(1);
    }
    exit(1);


}
