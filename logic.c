#include <stdio.h>
#include <stdlib.h>
#include "logic.h"
#include "files.h"
#include <SDL2/SDL.h>
#include <time.h>
#include <math.h>

SDL_Window *window = NULL;
SDL_Renderer* renderer = NULL;

int saveWorld(Grid grid) {
  if (grid.rows <= 0 || grid.cols <= 0 || grid.rows > MAXROWS || grid.cols > MAXCOLS) {
    return 0;
  }
  int choice;
  printf("Would you like to save this world? (1=yes, 2=no): ");
  scanf(" %i", &choice);
  if (choice == 1) {
    writeState(grid, "saved.txt");
    printf("World saved successfully!\n");
  }
  else {
    printf("World not saved.\n");
  }
  return 1;

}

//returns 0 if failed to run game, 1 if game runs successfully
int runGame(Grid gameGrid, int iterations) {

  if (gameGrid.rows <= 0 || gameGrid.cols <= 0 || gameGrid.rows > MAXROWS || gameGrid.cols > MAXCOLS) {
    return 0;
  }
  int oldest_cell = 0;
  if (SDL_Init(SDL_INIT_VIDEO) != 0)
    fprintf(stderr, "SDL_Init: %s\n", SDL_GetError());

  //atexit(SDL_Quit);

  const int window_size = 800;

  SDL_CreateWindowAndRenderer(window_size, window_size, 0, &window, &renderer);
  SDL_SetWindowTitle(window, "Game of Life");
  // SDL_SetRenderDrawColor(renderer, 0, 0, 0, 255);
  // SDL_RenderClear(renderer);
  // SDL_RenderPresent(renderer);

  for (int k = 0; k < iterations; k++) {
    for (int row = 0; row < gameGrid.rows; row++) {
        for (int col = 0; col < gameGrid.cols; col++) {
          SDL_Rect rect;
          rect.w = (window_size / gameGrid.rows)-1;
          rect.h = (window_size / gameGrid.rows)-1;
          rect.x = col*(window_size / gameGrid.rows);
          rect.y = row*(window_size / gameGrid.rows);
          if (gameGrid.arr[row][col] == 0) {
            SDL_SetRenderDrawColor(renderer, 255, 255, 255, 255);
          }
          else {
            if (gameGrid.arr[row][col]*4 >= 251) {
              SDL_SetRenderDrawColor(renderer, 0, 0, 0, 255);
            }
            else {
              SDL_SetRenderDrawColor(renderer, 0, 255-(gameGrid.arr[row][col]*4), 0, 255);
            }
            if (gameGrid.arr[row][col] > oldest_cell) {
              oldest_cell = gameGrid.arr[row][col];
            }
          }
          SDL_RenderFillRect(renderer, &rect);
        }
    }
    SDL_RenderPresent(renderer);
    SDL_Delay(80);
    nextGen(gameGrid);
  }
  //printf("Oldest cell = %i\n", oldest_cell);
  return 1;
}

Grid randomWorld(int rows, int cols) {
  srand(time(NULL));
  Grid grid = newGrid(rows, cols);
  int distribution = rand();
  //printf("Distribution = %i", distribution);
  for (int i = 0; i < rows; i++) {
    for (int j = 0; j < cols; j++) {
      if (rand() < distribution) {
        setAlive(grid, i, j);
      }
      else {
        setDead(grid, i, j);
      }
    }
  }
  return grid;
}

int coordinateCheck(Grid grid, int row, int col) {
    if (row < 0 || row >= grid.rows || col < 0 || col >= grid.cols) {
        return 0;
    }
    return 1;
}

int isAlive(Grid g, int row, int col) {
    if (coordinateCheck(g, row, col) == 0) {
        return 0;
    }
    if (g.arr[row][col] >= 1) {
        return 1;
    }
    else {
        return 0;
    }
}

int aliveNeighbours(Grid g, int row, int col) {
    if (coordinateCheck(g, row, col) == 0) {
        printf("Coordinate out of bounds! Cannot check alive neighbours.\n");
        exit(1);
    }
    int count = 0;
    count += isAlive(g, row, col-1);
    count += isAlive(g, row, col+1);
    count += isAlive(g, row-1, col-1);
    count += isAlive(g, row-1, col);
    count += isAlive(g, row-1, col+1);
    count += isAlive(g, row+1, col-1);
    count += isAlive(g, row+1, col);
    count += isAlive(g, row+1, col+1);
    return count;
}

void nextGen(Grid g) {

    int tempArr[g.rows][g.cols];
    for (int row = 0; row < g.rows; row++) {
        for (int col = 0; col < g.cols; col++) {
            tempArr[row][col] = g.arr[row][col];
        }
    }

    for (int row = 0; row < g.rows; row++) {
        for (int col = 0; col < g.cols; col++) {
            if (aliveNeighbours(g, row, col) <= 1) {
                tempArr[row][col] = 0;
            }
            else if (aliveNeighbours(g, row, col) == 3) {
                tempArr[row][col]++;
            }
            else if (aliveNeighbours(g, row, col) == 2 && tempArr[row][col] > 0)
            {
              tempArr[row][col]++;
            }
            else if (aliveNeighbours(g, row, col) > 3) {
                tempArr[row][col] = 0;
            }
        }
    }

    for (int row = 0; row < g.rows; row++) {
        for (int col = 0; col < g.cols; col++) {
            g.arr[row][col] = tempArr[row][col];
        }
    }


}

void printGen(Grid g, int num) {
    printGrid(g);
    for (int i = 0; i < num; i++) {
        nextGen(g);
    }
    printGrid(g);
}

void printGrid(Grid g) {
    printf("\n");
    for (int row = 0; row < g.rows; row++) {
        for (int col = 0; col < g.cols; col++) {
            printf("%i ", g.arr[row][col]);
        }
        printf("\n");
    }
    printf("\n");
}

int** newArray(int rows, int cols) {
    int** arr = malloc(sizeof(int*) * rows);
    for (int i = 0; i < rows; i++) {
        arr[i] = calloc(cols, sizeof(int));
    }
    return arr;
}


Grid newGrid(int rows, int cols) {
    Grid grid;
    if (rows <= 0 || cols <= 0 || rows > MAXROWS || cols > MAXCOLS) {
        printf("Invalid width or height for world!\n");
        grid.rows = 0;
        grid.cols = 0;
        return grid;
    }
    grid.rows = rows;
    grid.cols = cols;
    grid.arr = newArray(grid.rows, grid.cols);
    return grid;
}


int setAlive(Grid grid, int row, int col) {
    if (coordinateCheck(grid, row, col) == 0) {
        return 0;
    }
    grid.arr[row][col] = 1;
    return 1;
}

int setDead(Grid grid, int row, int col) {
    if (coordinateCheck(grid, row, col) == 0) {
        return 0;
    }
    grid.arr[row][col] = 0;
    return 1;
}
