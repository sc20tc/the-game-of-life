#define MAXROWS 100
#define MAXCOLS 100

typedef struct Grid {
    int** arr;
    int rows;
    int cols;
} Grid;

int coordinateCheck(Grid grid, int row, int col);
int isAlive(Grid g, int row, int col);
int aliveNeighbours(Grid g, int row, int col);
void nextGen(Grid g);
void printGrid(Grid g);
int** newArray(int rows, int cols);
Grid newGrid(int rows, int cols);
int setAlive(Grid grid, int row, int col);
int setDead(Grid grid, int row, int col);
void printGen(Grid g, int num);
Grid randomWorld(int rows, int cols);
int runGame(Grid gameGrid, int iterations);
int saveWorld(Grid grid);
