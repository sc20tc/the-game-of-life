(1) Git repository: https://gitlab.com/sc20tc/the-game-of-life

(2) See commit-history.jpg

(3) To compile program please compile using CMake by entering the following commands:

> mkdir build
> cd build
> cmake ..
> make

Then to run the program please use > ./game

Alternatively, if this does not work for any reason please complile manually with
> gcc main.c files.c logic.c -lSDL2 -lSDL2_image -std=c99 -o game
