#include <stdio.h>
#include <stdlib.h>
#include "logic.h"
#include "files.h"
#include <SDL2/SDL.h>

Grid readState(char* filename) {

    FILE* fp = fopen(filename, "r");
    if (fp == NULL) {
        perror("Failed to open file for reading! Please ensure a saved world exists and try again.\n");
        exit(1);
    }

    Grid grid;
    int count = 0;
    int rows;
    int cols;
    int row = 0;
    int col = 0;
    char chunk[5];
    while (fgets(chunk, sizeof(chunk), fp) != NULL) {
        if (count == 0) {
            rows = atoi(chunk);
        }
        else if (count == 1) {
            cols = atoi(chunk);
            //printf("Rows = %i, Cols = %i\n", rows, cols);
            grid = newGrid(rows, cols);
        }
        else {
            grid.arr[row][col] = atoi(chunk);
            col++;
            if (col == grid.cols) {
                col = 0;
                row++;
            }
            if (row >= grid.rows || col >= grid.cols){
                //printf("Breaking\n");
                break;
            }
        }
        count++;
    }
    fclose(fp);
    return grid;
}

int writeState(Grid g, char* filename) {
    FILE* fp = fopen(filename, "w");
    if (fp == NULL) {
        perror("Failed to open file for writing!\n");
        exit(1);
    }

    fprintf(fp, "%i\n", g.rows);
    fprintf(fp, "%i\n", g.cols);
    for (int row = 0; row < g.rows; row++) {
        for (int col = 0; col < g.cols; col++) {
            fprintf(fp, "%i\n", g.arr[row][col]);
        }
    }
    fclose(fp);
    return 1;
}
